# Licensed under the Apache License: http://www.apache.org/licenses/LICENSE-2.0
# For details: https://gitlab.com/mlrep/mldev/-/blob/master/NOTICE.md

import importlib.util as import_lib
import logging
import os

import yaml


class _SingletonWrapper:

    def __init__(self, cls):
        self.__wrapped__ = cls
        self.__dict__.update(cls.__dict__)
        self._instance = None

    def forget(self):
        """
        Purges current instance. A new instance is created on the next ''__call__''
        :return:
        """
        del self._instance
        self._instance = None

    def __call__(self, *args, **kwargs):
        if self._instance is None:
            self._instance = self.__wrapped__(*args, **kwargs)
        return self._instance


def singleton(cls):
    return _SingletonWrapper(cls)


@singleton
class MLDevSettings:

    DEFAULT_EXTRAS = {
        'base': 'mldev.experiment_objects',
        'bot': 'mldev_bot.bot_service',
        'tensorboard': 'mldev_tensorboard.tensorboard_service',
        'dvc': 'mldev_dvc.dvc_stage',
        'controller': 'mldev_controller.controller_service'
    }


    def default_config_path(self):
        """
        Returns current path to config.yaml for mldev
        :return: path to config
        """

        if hasattr(self, 'app_config_path') and self.app_config_path:
            return self.app_config_path

        home = os.path.expanduser("~")
        environ_app_config_path = os.environ.get('MLDEV_CONFIG_PATH', None)

        paths = [
            f"./.mldev/config.yaml",
            f"{home}/.config/mldev/config.yaml"
        ]

        if environ_app_config_path:
            paths.insert(0, environ_app_config_path)

        for p in paths:
            if os.path.exists(p):
                return p

        return paths[-1]

    def __init__(self, config_path=None):

        # this is relative to experiment dir
        self.temp_dir = ".mldev"
        # should point to tool installation dir
        # tool_dir == ./src
        self.tool_dir = os.path.abspath(os.path.dirname(__file__) + "/../")

        if not config_path:
            self.app_config_path = os.path.abspath(self.default_config_path())

            if not os.path.exists(self.app_config_path):
                os.makedirs(os.path.dirname(self.app_config_path), exist_ok=True)
                with open(self.app_config_path, "w+"):
                    pass
        else:
            if not os.path.exists(config_path):
                raise Exception(f"Config file not found: {config_path}")
            self.app_config_path = os.path.abspath(config_path)

        with open(self.app_config_path, "r+") as f:
            self.cfg = yaml.load(f, Loader=yaml.SafeLoader)
            if not self.cfg:
                self.cfg = dict()

        self.environ = self.cfg.get("environ", dict())
        self.environ['TOOL_DIR'] = self.tool_dir

        self.logs_dir = None
        if self.get_value("LOG_LEVEL") is None:
            self.set_value("LOG_LEVEL", logging.INFO)

        if "logger" in self.cfg:
            if "path" in self.cfg["logger"]:
                self.logs_dir = self.cfg["logger"]["path"]
                os.makedirs(self.logs_dir, exist_ok=True)

            if "level" in self.cfg["logger"]:
                if self.cfg["logger"]["level"] == "DEBUG":
                    self.set_value("LOG_LEVEL", logging.DEBUG)
                elif self.cfg["logger"]["level"] == "INFO":
                    self.set_value("LOG_LEVEL", logging.INFO)
                elif self.cfg["logger"]["level"] == "ERROR":
                    self.set_value("LOG_LEVEL", logging.ERROR)
                else:
                    raise Exception(f"Invalid log level: {self.cfg['logger']['level']}")

        logger = logging.getLogger("mldev.mldev_config_parser")
        logger.setLevel(self.get_value("LOG_LEVEL"))

        self._extras = self.cfg.get("extras", MLDevSettings.DEFAULT_EXTRAS)
        logger.info(f"Loading extras: {self._extras.keys()}")
        for extra in self._extras:
            module_name = str(self._extras[extra])
            path = os.path.join(self.tool_dir, module_name.replace(".",os.path.sep) + ".py")
            logger.info(f"Trying to load extra {extra} with {module_name} from {path}")
            if os.path.exists(path):
                _import_module(path, module_name)
            else:
                # try __init__.py
                path = os.path.join(self.tool_dir,
                                    module_name.replace(".", os.path.sep),
                                    os.path.sep, "__init__.py")
                logger.info(f"Trying to load extra {extra} with {module_name} from {path}")
                if os.path.exists(path):
                    _import_module(path, module_name)
                else:
                    raise ModuleNotFoundError(module_name)

        self.__class__._instance = self

    def get_value(self, name):
        return self.environ.get(name, None)

    def set_value(self, name, value):
        if value is None:
            del self.environ[name]
        else:
            self.environ[name] = value

    def is_feature(self, name):
        return self.environ.get(name, "False") == "True"

    def is_extra(self, name):
        return name in self._extras

    def get_extra(self, name):
        return self._extras.get(name)

    def get_extra_base(self, name):
        return str(self._extras.get(name, "")).split('.')[0]

    def set_feature(self, name, value):
        if value is None:
            self.environ[name] = None
        else:
            self.environ[name] = "True" if bool(value) else "False" if not bool(value) else None


def _import_module(path, module_name):
    spec = import_lib.spec_from_file_location(module_name, path)
    module = import_lib.module_from_spec(spec)
    spec.loader.exec_module(module)