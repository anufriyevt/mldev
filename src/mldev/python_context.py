# Licensed under the Apache License: http://www.apache.org/licenses/LICENSE-2.0
# For details: https://gitlab.com/mlrep/mldev/-/blob/master/NOTICE.md

import importlib.util as import_lib

from mldev.experiment_tag import _scalar_representer, _scalar_loader, experiment_tag
from mldev.expression import Expression


def load_function(path, module_name, object_name):
    spec = import_lib.spec_from_file_location(module_name, path)
    module = import_lib.module_from_spec(spec)
    spec.loader.exec_module(module)
    return module.__getattribute__(object_name)


@experiment_tag(loader=_scalar_loader, representer=_scalar_representer, name="function")
class PythonFunction:
    """
    Use this to load a specific python function into object graph from experiment.yaml

    This is a string scalar specifying a fully qualified name of the function
    """
    def __new__(cls, *args, **kwargs):
        function_name = str(Expression(args[0]))

        module_name = ".".join(function_name.split(".")[:-1])
        path = f"{module_name}.py"
        import_name = function_name.split(".")[-1]

        return load_function(
            path=path,
            module_name=module_name,
            object_name=import_name
        )
