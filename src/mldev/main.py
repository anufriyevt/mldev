# Licensed under the Apache License: http://www.apache.org/licenses/LICENSE-2.0
# For details: https://gitlab.com/mlrep/mldev/-/blob/master/NOTICE.md

import argparse
import os, sys
import logging

from mldev_config_parser.config_parser import MLDevSettings
from mldev.yaml_loader import YamlLoaderWithEnvVars
from mldev.utils import exec_tool_command
from mldev import logger


def run_experiment(experiment="./experiment.yml", pipeline="pipeline"):
    """
    Run experiment from the :param experiment: file

    :param experiment:
    :param pipeline:

    :returns:
    """
    from mldev.experiment import GenericPipeline

    logger.info(f"Loading {experiment}")
    config_loader = YamlLoaderWithEnvVars(experiment)
    experiment_config = config_loader.load_config()
    stages = experiment_config.get(pipeline)

    logger.info(f"Running {pipeline} from {experiment}")
    if not callable(stages):
        stages = GenericPipeline(runs=stages)

    stages(experiment_config, mode="Preparing")
    stages(experiment_config, mode="Running")


def init(args):
    """
    Implement `mldev init` command

    :param args:
    :return:
    """
    MLDevSettings().set_feature('MLDEV_NOCOMMIT', args.no_commit)

    env = {}
    if MLDevSettings().is_feature("MLDEV_NOCOMMIT"):
        env['MLDEV_NOCOMMIT'] = "True"

    folder = os.path.abspath(args.folder)

    if args.template is not None:
        exec_tool_command(f'init_template.sh "{folder}" "{args.template}"', environ=env)
    else:
        if args.reuse:
            exec_tool_command(f'init_template.sh "{folder}"', environ=env)
        else:
            exec_tool_command(f'init_template.sh "{folder}" -', environ=env)

    curr_dir = os.curdir
    try:
        # try reload config from the template
        # if there was a cmd line arg it will be reused
        os.chdir(f"{folder}")
        MLDevSettings.forget()

        exec_tool_command(f'init_venv.sh .', environ=env)

        if MLDevSettings().is_extra('dvc'):
            extra_package = MLDevSettings().get_extra_base('dvc')
            exec_tool_command(f'init_dvc.sh .', extra=extra_package, environ=env)

        logger.info("Initialization has been done")

    finally:
        os.chdir(curr_dir)


def run(args):
    """
    Implements `mldev run` command

    :param args:
    :return:
    """
    MLDevSettings().set_feature('MLDEV_NOCOMMIT', args.no_commit)
    MLDevSettings().set_feature('FORCE_RUN', args.force_run)

    # this imports stages module from the .mldev folder (should be in PYTHONPATH)
    mldev_stages = os.path.abspath("./.mldev")
    logger.info(f"Current sys.path is {str(sys.path)}")
    if mldev_stages not in map(os.path.abspath, sys.path):
        sys.path.append(mldev_stages)
    try:
        import stages
    except ModuleNotFoundError as err:
        logger.warn(f"Custom stages are not found in {mldev_stages}", exc_info=err)

    run_experiment(experiment=args.file, pipeline=args.pipeline)


def urls(args):
    exec_tool_command(f'ngrok_urls.sh')


def _version(args):
    from mldev.version import __version__
    print(__version__)


def setup_logging():
    """
    Inits logging for mldev when run as a tool

    :return:
    """
    logger.setLevel(MLDevSettings().get_value("LOG_LEVEL"))

    if MLDevSettings().logs_dir is not None:
        test_logs_path = MLDevSettings().logs_dir
        os.makedirs(test_logs_path, exist_ok=True)

        fh = logging.FileHandler(test_logs_path + "/debug.log")
        fh.setFormatter(logging.Formatter(logging.BASIC_FORMAT))
        fh.setLevel(logging.DEBUG)

        logger.addHandler(fh)


def do_main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--config", type=str, help="path to config.yaml file")

    subparsers = parser.add_subparsers(help='List of commands')
    version_parser = subparsers.add_parser("version", help="Prints current version")

    init_parser = subparsers.add_parser(
        "init",
        help="Creates the directories structure, starts venv, configure dvc in the specified folder"
    )
    run_parser = subparsers.add_parser(
        "run",
        help="starts mldev tool in the current folder"
    )

    urls_parser = subparsers.add_parser(
        "urls",
        help="get externally accessible urls for services"
    )

    init_parser.add_argument(
        "-t", "--template",
        type=str,
        help="you may pass preferable template to organize your project properly. "
              "If it is not given then template will be set to the template-default. "
              "(tip: see https://gitlab.com/mlrep for more information")
    init_parser.add_argument(
        "folder",
        type=str,
        help="you must specify folder for the mldev initialization"
    )

    init_parser.add_argument(
        "-r", "--reuse",
        action="store_true",
        help="set this key if you want to reuse an existing folder with your code"
    )

    run_parser.add_argument("--no-commit", action="store_true",
                             help="Disables committing data configs to Git")
    run_parser.add_argument("-f", "--file", default="experiment.yml",
                             help="Specify experiment file to use")
    run_parser.add_argument("pipeline", default="pipeline", nargs='?',
                             help="Set pipeline to run from the experiment")
    run_parser.add_argument("--force-run", action="store_true", default=False,
                            help="Force running the experiment regardless of any cached intermediate results")

    init_parser.add_argument("--no-commit", action="store_true",
                             help="Disables committing data configs to Git")

    init_parser.set_defaults(func=init)
    run_parser.set_defaults(func=run)
    version_parser.set_defaults(func=_version)
    urls_parser.set_defaults(func=urls)

    args = parser.parse_args()
    if args.config:
        # pre-create MLDevSetting with the config
        MLDevSettings(args.config)

    setup_logging()
    logger.info(f"Starting mldev in {os.path.abspath(os.curdir)}")

    if not hasattr(args, 'func') or not args.func:
        parser.print_help()
        parser.exit()

    args.func(args)


if __name__ == "__main__":
    do_main()
