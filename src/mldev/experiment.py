# Licensed under the Apache License: http://www.apache.org/licenses/LICENSE-2.0
# For details: https://gitlab.com/mlrep/mldev/-/blob/master/NOTICE.md

import time
from pathlib import Path

from mldev.experiment_tag import experiment_tag
from mldev.utils import *
from mldev import *

from mldev.yaml_loader import stage_context

from mldev.experiment_tag import experiment_tag
from mldev.python_context import PythonFunction


@experiment_tag()
class BasicStage(object):
    """
    A main class for non-versioned pipeline stages

    Accepts the following parameters:

        `inputs` - lists files and folders that this stage depends upon

        `outputs` - lists files and folders that this stage produces
    and that will be added to version control

        `params` - parameters for the commands being invoked

        `env` - additional environmental variables to pass to commands

        `script` - a list of command to invoke
    """

    def __init__(self, name="", params={}, env={}, inputs={}, outputs={}, script=[]):
        super().__init__()
        self.name = name
        self.params = params
        self.env = env
        self.inputs = inputs
        self.outputs = outputs
        self.script = script

    def __repr__(self):
        with stage_context(self):
            return str(self.__dict__)

    def __call__(self, *args, **kwargs):
        with stage_context(self):
            stage_name = args[0] if args[0] else self.name
            logger.debug(f"{self.__class__.__name__}({stage_name}).__call__()")
            if stage_name:
                if not MLDevSettings().is_feature("FORCE_RUN"):
                    output_min, output_max, outputs_missing = self._get_modified_range(self.outputs)
                    input_min, input_max, inputs_missing = self._get_modified_range(self.inputs)

                    if not inputs_missing and output_min > input_max:
                        # assume nothing changed
                        logger.info(f"Unchanged ({stage_name})")
                        return

                self.run(stage_name)

    def _get_modified_range(self, paths):

        def get_path_times(path, min_ts=time.time(), max_ts=0):

            missing = False
            try:
                file_ts = os.path.getmtime(str(path))
                min_ts = min(file_ts, min_ts)
                max_ts = max(file_ts, max_ts)

                for f in Path(str(path)).rglob("*"):
                    try:
                        file_ts = os.path.getmtime(str(f))
                        min_ts = min(file_ts, min_ts)
                        max_ts = max(file_ts, max_ts)
                    except OSError:
                        pass
            except OSError:
                missing = True

            return min_ts, max_ts, missing

        min_ts = time.time()
        max_ts = 0

        missing = True
        for file_path in paths:
            if isinstance(file_path, FilePath):
                for file in file_path.get_files(start=os.path.curdir):
                    min_ts, max_ts, missing = get_path_times(file, min_ts, max_ts)

                if len(file_path.files) == 0:
                    min_ts, max_ts, missing = get_path_times(file_path.path, min_ts, max_ts)

            else:
                min_ts, max_ts, missing = get_path_times(file_path, min_ts, max_ts)

        return min_ts, max_ts, missing

    def run(self, stage_name):
        with stage_context(self):
            capture = not (MLDevSettings().get_value("LOG_LEVEL") < LOG_INFO)

            script = prepare_experiment_command(' && '.join([str(s) for s in self.script]),
                                                    env=self.env)
            exec_command(script, capture=capture)


@experiment_tag()
class MonitoringService(object):
    """
    A common superclass for services accompanying the experiment

    """

    def __init__(self, name=None, params={}):
        super().__init__()
        self.name = name
        self.params = params
        self.error_file = f"{MLDevSettings().temp_dir}/"
        self.config_dir = MLDevSettings().tool_dir
        self.temp_dir = MLDevSettings().temp_dir

    def __repr__(self):
        return "{}(name={}, params={})".format(
            self.__class__.__name__,
            self.name,
            self.params
        )

    def __call__(self, service_name):
        pass

    def prepare(self, service_name):
        pass


@experiment_tag()
class GenericPipeline(object):
    """
    This is a basic pipeline to run stages and services in sequence

    Supports the following modes of operation:
        - sequence of runs - then use `runs` attribute in experiment.yml

    When using runs, it executes 'prepare' and then calls the items in the rder specified

    """
    def __init__(self, *args, **kwargs):
        super().__init__()
        self.runs = kwargs.get('runs', [])
        self.stages = kwargs.get('stages', [])
        self.services = kwargs.get('services', [])
        pass

    def __call__(self, experiment, mode="Running"):
        if mode == "Preparing":
            self.exec_stages(experiment, mode)
            self.exec_runs(experiment, mode)
        if mode == "Running":
            self.run_services(experiment)
            self.exec_stages(experiment, mode)
            self.exec_runs(experiment, mode)
        if mode is None:
            self.exec_runs(experiment, mode)
        pass

    def run_services(self, experiment_config):
        for service in self.services:
            service()

    def exec_stages(self, experiment_config, exec_type):
        for stage_name in self.stages:
            current_stage = experiment_config.get(stage_name)
            logger.info(f"{exec_type}: {stage_name}")
            current_stage(stage_name)

    def exec_runs(self, experiment_config, exec_type):
        for run in self.runs:
            run_name = ""
            if hasattr(run, "name"):
                run_name = run.name
            logger.info(f"{self.__class__.__name__} {exec_type}: {run_name}")
            if (exec_type is None or exec_type == "Preparing") and hasattr(run, "prepare"):
                if callable(run.prepare):
                    run.prepare(run_name)
            if exec_type is None or exec_type == "Running":
                run(run_name)


@experiment_tag(name="path")
class FilePath:
    """
    Implements a collection of files prefixed with a common path
    """

    def __init__(self, *args, **kwargs):
        self.path = kwargs.get("path", "")
        some_files = kwargs.get("files", [])
        self.files = some_files if some_files else []

    def get_files(self, start=None):
        if start is None:
            return [os.path.abspath(
                os.path.join(str(self.path), str(x))) for x in self.files if x is not None]
        else:
            return [os.path.relpath(
                os.path.join(str(self.path), str(x)), start) for x in self.files if x is not None]

    def get_path(self, start=None):
        if start is None:
            return os.path.abspath(str(self.path))
        else:
            return os.path.relpath(str(self.path), start)

    def __str__(self):
        return self.get_path(start=os.path.curdir) \
            if len(self.files) == 0 \
            else " ".join([str(x) for x in self.get_files(start=os.path.curdir)])

    def __repr__(self):
        return self.__str__()

    def __eq__(self, other):
        return str(self) == str(other)

    def to_json(self):
        return self.get_files()


__all__ = ['MonitoringService',
           'experiment_tag',
           'PythonFunction',
           'GenericPipeline',
           'BasicStage',
           'FilePath',
           'MLDevSettings']