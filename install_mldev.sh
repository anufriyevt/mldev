#!/bin/bash

# Licensed under the Apache License: http://www.apache.org/licenses/LICENSE-2.0
# For details: https://gitlab.com/mlrep/mldev/-/blob/master/NOTICE.md

set -o pipefail

# enable debug
#set -x
echo "(mldev) Downloading mldev package file ..."
PACKAGE_URL="https://gitlab.com/mlrep/mldev/-/package_files/7876824/download"
PACKAGE_NAME=$(curl --remote-name --remote-header-name --write-out "%{filename_effective}" --silent ${PACKAGE_URL})

read -rp "(mldev) Install system libraries (python3, git, ...)? [Y|n]: " -n 1
echo
if [[ "$REPLY" =~ (^[Yy]$|^$) ]]; then
  echo "(mldev) Downloading mldev pre-requisites install script ..."
  echo
  curl --remote-name --remote-header-name --silent https://gitlab.com/mlrep/mldev/-/raw/develop/install_reqs.sh && chmod +x ./install_reqs.sh

  # call in a separate process (may require root)
  echo "(mldev) Installing system libraries. You shall be root or have sudo"
  echo
  ./install_reqs.sh
fi

# install mldev into default location
pip3 install --upgrade pip setuptools wheel

extras="$(IFS=, ; echo "$*")"
echo "(mldev) Installing mldev with the following extras: $extras"
pip3 install mldev[$extras]@file://${PWD}/${PACKAGE_NAME}

echo "(mldev) Done!"