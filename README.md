# MLDev software

This repository contains the MLDev software, that facilitates running data science experiments, 
help in results presentation and eases paper preparation for students, data scientists and researchers.

The MLDev software provides the following features to help with automating machine learning experiments:
 - Configure stages and parameters of a data science experiment separately from your code
 - Conduct a repeatable experiment in Google Colab or PaperSpace
 - Keep versions of your code, results and intermediate files on Google Drive (other repos coming soon)
 - Use a variety of pre-built templates to get started: see [template-default](../../../../template-default) 
 and [template-intelligent-systems](../../../../template-intelligent-systems)

MLDev also provides some services that run alongside your experiment code:
You can have the notifications via Telegram about the exeptions while training your model
 - Keep updated on current experiment state using TensorBoard (even on Colab)
 - Deploy and demo your model with a model controller (feature in progress) 

# Installation

## Pre-requisites

You will need the following:
1. A Git account at a publicly accessible repo hosting provider Github/Gitlab or your own (a Gitlab account is needed for pipeline automation)
2. A Google account - to run experiments in the cloud
3. A Google account to save experiment data
4. An ngrok.io account for Tensorboard access
5. A Telegram account to receive notifications from the bot

## Install system packages

This is needed if you are going to run mldev on raw ``ubuntu:18.04`` or other Docker container images.

A list of system packages to be installed is provided in the [``install_reqs.sh``](./install_reqs.sh). 
This script is run if needed during installation.

## Install mldev

Get the latest version of our install file to your local machine and run it.

```shell script
$ curl https://gitlab.com/mlrep/mldev/-/raw/develop/install_mldev.sh -o install_mldev.sh 
$ chmod +x ./install_mldev.sh
$ ./install_mldev.sh core
```
You may be asked for ``root`` privileges if there are [system packages to be installed](#install-system-packages)

Wait a couple of minutes until installation will be done and then you are almost ready to use our instrument, congrats!

## Alternative sources

It is also possible to install mldev from [a test PyPI repostory](https://test.pypi.org/simple)

```shell script
$ pip install -i https://test.pypi.org/simple mldev
```

Note: this source is subject to test PyPI repo constraints on availability

## Configuration files

MLdev may use ``config.yaml`` file for its configuration. Check this wiki page for detail on [mldev configuration files](../wikis/Конфигурационный-файл-и-переменные-по-умолчанию). 

## Installing extras

mldev provides the following extra dependencies which can be installed if needed

- **bot** - installs a Telegram notification bot, configured as ``!NotificationService`` service
- **controller** - installs dependencies for Flask-based model serving, use as ``!ModelController`` service
- **tensorboard** - for tracking experiment progress using Tensorboard, add as ``!TensorBoardService`` service
- **dvc** - adds data version controlled stages using DVC and git, configure as ``!Stage``
- **base** - is the most basic version, uses ``!BasicStage`` without version control

You can install them like this
```shell script
$ ./install_mldev.sh core bot dvc tensorboard controller
```

When installed, you can add them in the ``config.yaml``:
```yaml
extras:
  base: mldev.experiment_objects
  bot: mldev_bot.bot_service
  tensorboard: mldev_tensorboard.tensorboard_service
  dvc: mldev_dvc.dvc_stage
  controller: mldev_controller.controller_service
```
Note that values here refer to modules that contain definitions of the correponding stages and services.

See examples of each in [template-default](../../../../template-default) and in [test config.yaml](test/config.yaml)


# Quick guide

## Experiment setup

Experiment constists of stages, services, pipelines and any other custom object you would like to add.

**Stages** are unit of the experiment that need their inputs and outputs versioned. 
For example, for repoducibility reasons. MLdev currently supports DVC stages.

**Pipelines** are sequences of stages and service that can be run as a single experiment

**Services** are anything that does not produce results but help running the experiment. 
For instance, a Tensorboard running alongside to display training progress is a service.

Experiment description is usually located in ``experiment.yml`` file. 
You can specify another file using ``-f`` switch like this:
```shell script
$ mldev run -f <path/to/experiment.yml> <pipeline name>
```

Check some examples in [templates](../../../../)  

Here we provide an instruction on making a new experiment using our instrument.
You can get help on MLdev command anytime by typing ``mldev --help``

### Step 1. Create a separate project for the experiment

Initialize mldev in the current folder:

```shell script
$ mldev init <your_folder>
```

You use one of the templates (learn about mldev templates [here](../../wikis/Mldev-Templates)).
Here are valid examples of using a template. 

```shell script
$ mldev init <your_folder> -t template-default
```
```shell script
$ mldev init <your_folder> -t https://github.com/<path-to-template>
```

Or just don't use ``-t`` flag, template will be set to ``template-default`` automatically.

During initialization you may be asked about a new URL for the project 
and your Git login and email. 
This is needed to put your new experiment code under version control.
If you do not need this, add the ``--no-commit`` switch like this:

```shell script
$ mldev init --no-commit <your_folder>
``` 
 
You can also reuse an existing folder where you experiment is stored. 
Set ``-r`` as in here:

```shell script
$ mldev init -r <your_folder>
```

At this step DVC might ask you about Google Drive folder id for storing experiment data.
This is usually a hexademical string - the last part of the Google Drive folder URL. 
For example, in the URL ``https://drive.google.com/drive/folders/1SqqJB9eDk822GNgyJ2PQWMy_IG1AFhWO`` 
the requested folder id will be ``1SqqJB9eDk822GNgyJ2PQWMy_IG1AFhWO`` 

See [DVC configuration](../../wikis/DVC_configuration) for more details.

Do not forget to step into the new experiment folder! 

```shell script
$ cd <your_folder>
```

### Step 2. Configure your experiment

Configure your experiment with [experiment.yml](../wikis/new-format-of-experiment.yml-configuration#monitoring-services)

### Step 3 - Commit changes

By default MLdev will add and commit necessary configuration files to the new Git repo.
You may review them before pushing:

```shell script
$ git log
$ git push
```

Done!

## Running the experiment (locally)

If you have followed all the steps above, you can now run your experiment:

```shell script
$ mldev run
```

You can add ``--no-commit`` switch to skip adding results to version control.

## Running the experiment (Google Colab)

Open a Google Colab notebook and add and run the following lines in the beginning (assuming a Gitlab account).
Note that commands start with the exclamation sign. Example is done for the gitlab, if you use another storage please change `gitlab.com` in the line below to your storage.

```shell script
!git clone https://<user>:<password>@gitlab.com/<user>/<experiment repo>.git
```


then you will need to install mldev on Google colab (please do Experiment Setup Step1 on Google Colab)

Add and run this lines to run the experiment

```shell script
cd <your_project_folder>
!mldev run
```

## Advanced usage

### Using expressions in the experiment configuration

Instead of constant strings, a calculable python expressions can be used in 
experiment configuration.

In this example, we use a full path of the output as a command line parameter
in the script 

```yaml
prepare: &prepare_stage !Stage
  name: prepare
  params:
    size: 1
  outputs:
    - !path
      path: "./data"
      files:
        - "first.txt"
    - !path
      path: "./logs"
  script:
    - >
      python3 src/prepare.py 
              --to \"${self.outputs[0].get_files()[0]}\" 
              --log \"${path(self.outputs[1])}\"
```

Here everything inside ``${...}`` is an **expression**. 
Expressions are evaluated at the time they are used (runtime) using a restricted subset of ``python`` language. 
Expressions are required to return a value.

In the example above the following script will be run for the stage 
(note the ``/home/user/projects/template-mldev`` prepended):
```shell script
$ python3 src/prepare.py \ 
          --to "/home/user/projects/template-mldev/data/first.txt" \
          --log "/home/user/projects/template-mldev/logs"
```

The following variables are available to expressions:
 - ``self`` points to the current ``Stage``, ``Pipeline`` or ``Service``
 - ``root`` points to the yaml document itself
 - ``env`` contains a dictionary of environment variables avaialble to mldev 
 as well as ``environ`` from the mldev config, see [mldev configuration](#configuration-files) for more details.

These functions can be used:
 - ``json(obj)`` converts a dictionary, list or scalar value to a JSON representation and escapes it.
 - ``path(str)`` expands ``str`` to the full path
  

### Using custom types

MLdev provides the following custom types or tags (yaml-y speaking), which can be used to describe an experiment
 - ``!path`` creates a ``FilePath`` object
 - ``!function`` imports a ``python`` function or class, which can be used in expressions
  
More details on configuring stages and services are given in [reference doc for experiment.yml](../wikis/new-format-of-experiment.yml-configuration#monitoring-services)

**Security considerations**

Using ``!function`` allows to import any python function or class that python import subsystem can load.

### Telegram notifications

In order to use telegram notification bot you will need to obtain your personal token. Please visit: [Telegram token](../wikis/instructions-for-tokens-obtaining-for-monitoring-services#telegram-bot)

Open Telegram using your account via link [Telegram](https://veb.telegram.org)
Open your Telegram Bot and type 

`/start`

enjoy logs via telegram while having a cup of tea!

### Using Tensorboard on Google Colab

In order to use Tensorboard you will need to obtain ngrok token - please visit: [ngrok token](../wikis/instructions-for-tokens-obtaining-for-monitoring-services#tensorboard)

 - Open your Ngrok DashBoard via link: [Ngrok](https://ngrok.com)
 - or add and run this commands to the Google Colab notebook:

```shell script
!chmod ugo+x ./src/ngrok_urls.sh
!./src/ngrok_urls.sh
```


### Model demo

Optional, to publish your model with flask controller please visit: [Flask Controller Instruction](../../wikis/Flask-Controller-Instructions)


### Running pipelines through Gitlab

*This functionality is still under development and will be here soon*

It is possible to run an experiment via Gitlab CI/CD. Detailed instructions will be provided when available.
See [template-default](../template-default/.gitlab-ci.yml) for an example.
    
# Contributing

Please check the [CONTRIBUTING.md](CONTRIBUTIONG.md) guide if you'd like to participate in the project, ask a question or give a suggestion.

# Project sponsors and supporters

<p><a href="https://fund.mipt.ru"><img height="100px" src="../../wikis/images/fund-logo.svg" alt="MIPT Fund"/></a>
<a href="https://mipt.ru"><img src="https://mipt.ru/docs/download.php?code=mipt_eng_base_png" alt="MIPT" height="200px"/></a>
<a href="https://mipt.ru/education/departments/fpmi/"><img src="https://mipt.ru/docs/download.php?code=logotip_fpmi_2019" height="200px" alt="FPMI"/></a>
<a href="http://www.machinelearning.ru"><img src="http://www.machinelearning.ru/wiki/logo.png" alt="MachineLearning.ru" height="120px"/></a>
<a href="http://m1p.org"><img src="../../wikis/images/m1p_logo.png" alt="My First Scientific Paper" height="120px"></a>
<a href="http://fpmi.tilda.ws/algo-tech/"><img src="../../wikis/images/atp-mipt.jpg" alt="ATP MIPT" height="120px"></a></p>

# Contacts 

You can reach developers at the [Telegram user group](https://t.me/mldev_betatest)

# License

The software is licensed under [Apache 2.0 license](LICENSE)
