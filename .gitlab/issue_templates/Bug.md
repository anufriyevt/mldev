## Резюме

Краткое описание дефекта

## Конфигурация

Версия MLDev: xx.xx (или коммит)
Python: x.y
OS: <название ОС>

## Как повторить

- шаг 1
- шаг 2

## Описание дефекта

Что делает mldev:

Что должен делать:

Почему:

## Как провобали решить

Как можно решить проблему уже сейчас?

Есть ли сходные проблемы (дать ссылку)?

## Приложения

Добавьте скриншоты или копии сообщений об ошибке, это поможет быстрее понять в чем причина